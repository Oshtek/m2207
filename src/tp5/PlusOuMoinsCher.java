package tp5;

import java.awt.event.*; //Utiliser des evenements
import java.awt.Container; //Utiliser Container de fenetre et Composants graphiques
import java.awt.GridLayout; //Utiliser GridLayout

import javax.swing.JButton; //Utiliser des boutons
import javax.swing.JFrame; //Utiliser les fonctions de la classe JFrame
import javax.swing.JLabel; //Utiliser des labels
import javax.swing.JTextField; //Utiliser les TextField


public class PlusOuMoinsCher extends JFrame implements ActionListener{
	//Attributs
	private JTextField proposition=new JTextField();
	private JLabel monLabel=new JLabel("Votre Proposition :");
	private JButton verifier=new JButton("Verifier !");
	private JLabel reponse=new JLabel ("La Reponse");
	private int chiffre=  0 + (int)(Math.random() * ((100 - 0) + 1)); //Nombre aleatoire entre 0 et 100
	private int essai=8;

	//Accesseurs
	int getChiffre(){
		return chiffre;
	}
	int getEssai(){
		return essai;
	}

	//Constructeurs
	public PlusOuMoinsCher(){
		super(); //Fait appel au constructeur de la classe JFrame
		this.setTitle("Plus cher ou moins cher ?");
		this.setSize(350,150); //Par defaut, fenetre de taille 350x150
		this.setLocation(300,150); //Position (300,150) de l'ecran
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //gestion fermeture de fenetre
		System.out.println("La fenetre a ete creee !"); //Petit message de confirmation lors de la creation de fenetre
		Container panneau = getContentPane(); //Container
		panneau.add(monLabel);
		panneau.add(proposition);
		panneau.add(verifier);
		panneau.add(reponse);
		panneau.setLayout(new GridLayout(2,2)); //Grille en (2,2)
		verifier.addActionListener(this); //Ecoute de l'action sur ce bouton
		this.setVisible(true); //Obligatoire en fin de constructeur
	}



	//Executable
	public static void main(String[] args) {
		PlusOuMoinsCher app = new PlusOuMoinsCher();
	}

	//Methodes
	public void actionPerformed(ActionEvent e) {
		if(Integer.parseInt(proposition.getText())>getChiffre()){
			essai=essai-1; //Decremente le nombre d'essai en cas d'erreur
			System.out.println("Moins cher ! Essai(s) restant(s) : " + getEssai());
		}
		else if (Integer.parseInt(proposition.getText())<getChiffre()){
			essai=essai-1; //Decremente le nombre d'essai en cas d'erreur
			System.out.println("Plus cher ! Essai(s) restant(s) : " + getEssai());
		}
		else {
			System.out.println("Juste prix ! Felicitation !");
			init();
		}
		fin();
	}
	void fin(){
		if (essai==0){
			System.out.println("Perdu ! le bon chiffre etait " + getChiffre());
			init();
		}
	}
	public void init(){ //Reinitialiser le jeu
		chiffre=  0 + (int)(Math.random() * ((100 - 0) + 1)); //Nombre aleatoire entre 0 et 100
		essai=8;
	}
}
