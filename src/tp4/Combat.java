package tp4;

public class Combat{

	public static void main(String[] args) {
		Pokemon miaoutou;
		miaoutou=new Pokemon("Miaoutou");
		Pokemon bulbizarre; //Nouveau pokemon bulbizarre
		bulbizarre=new Pokemon("Bulbizarre");

		miaoutou.sePresenter(); //Miaoutou se presente
		bulbizarre.sePresenter(); //bulbizarre se presente
		miaoutou.combat(miaoutou, bulbizarre); //Miaoutou et Bulbizarre se combattent
	}
}
