package td1;

import java.util.Scanner;

public class Exercice4 {
	static int random;
	static int nombre;
	public static void main(String[] args) {
		Scanner sc= new Scanner(System.in);
		int random=(int)(Math.random()*21);
		System.out.println("Veuillez saisir un nombre entre 0 et 20 : ");
		int nombre= sc.nextInt();
		while (nombre>=21 || nombre<0){
			System.out.println("Veuillez saisir un nombre entre 0 et 20 : ");
			nombre= sc.nextInt();
		}
		while (nombre>random){
			System.out.println("Plus petit !");
			nombre= sc.nextInt();
		}
		while (nombre<random){
			System.out.println("Plus grand !");
			nombre= sc.nextInt();
		}
		if (nombre==random){
			System.out.println("Bravo vous avez gagn� !");
		}
	}

}

/* Pour generer un nombre aleatoire dans une intervalle precise, il faudra utiliser la syntaxe suivante : (Math.Random()*(NbMax-NbMin+1))+NbMin */