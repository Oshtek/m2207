package tp5;

import java.awt.BorderLayout; //Utiliser BorderLayout
import java.awt.Container; //Utiliser Container de fenetre et Composants graphiques
import java.awt.FlowLayout; //Utiliser Layout Manager
import java.awt.event.*; //Utiliser des evenements

import javax.swing.JButton; //Utiliser des boutons
import javax.swing.JFrame; //Utiliser les fonctions de la classe JFrame
import javax.swing.JLabel; //Utiliser des labels



public class CompteurDeClic extends JFrame implements ActionListener{ //Herite de JFrame et ajout de l'interface ActionListener
	//Attributs
	private JButton monBouton=new JButton("Click !");
	private int cpt=0;
	private JLabel monLabel;

	//Accesseurs
	int getCpt(){
		return cpt;
	}

	//Constructeurs
	public CompteurDeClic(){
		super(); //Fait appel au constructeur de la classe JFrame
		this.setTitle("Click counter !");
		this.setSize(200,100); //Par defaut, fenetre de taille 200x100
		this.setLocation(20,20); //Position (20,20) de l'ecran
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //gestion fermeture de fenetre
		System.out.println("La fenetre a ete creee !"); //Petit message de confirmation lors de la creation de fenetre
		Container panneau = getContentPane(); //Container
		panneau.add(monBouton, BorderLayout.NORTH);
		monBouton.addActionListener(this); //Ecoute de l'action sur ce bouton
		monLabel=new JLabel("Vous avez cliqu� " + getCpt() + " fois !"); //Creation du label
		panneau.add(monLabel, BorderLayout.SOUTH);
		this.setVisible(true); //Obligatoire en fin de constructeur
	}

	//Executable
	public static void main(String[] args) {
		CompteurDeClic app = new CompteurDeClic();
	}

	//Methodes
	public void actionPerformed(ActionEvent e) {   
		System.out.println("Une action a �t� d�tect�e");
		cpt++; //A chaque clic, le compteur est incremente
		monLabel.setText("Vous avez cliqu� " + getCpt() + " fois !"); //Mise a jour du label
	}
}
