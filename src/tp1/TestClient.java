package tp1;

public class TestClient {

	public static void main(String[] args) {
		Client c;
		Compte compte1;
		compte1=new Compte(42);
		c=new Client("Sarce","Yves",compte1); //Creer nouveau client auquel on attribue un compte
		System.out.println("nom=" + c.getNom()); //Afficher le nom du client
		System.out.println("prenom=" + c.getPrenom()); //Afficher le prenom du client
		c.getSolde(); //Recuperer solde du compte en banque
		compte1.depot(100); //depot de 100 euros sur son compte
		c.afficherSolde(); //Afficher le solde du compte

		System.out.println("\nExercice4");
		//Creation des divers comptes et depot
		Compte c3,c4,c5,c6,c7,c8,c9,c10,c11,c12;
		c3= new Compte(3);
		c3.depot(420);
		c4= new Compte(4);
		c4.depot(729);
		c5= new Compte(5);
		c5.depot(2400);
		c6= new Compte(6);
		c6.depot(1650);
		c7= new Compte(7);
		c7.depot(1350);
		c8= new Compte(8);
		c8.depot(1275);
		c9= new Compte(9);
		c9.depot(180);
		c10= new Compte(10);
		c10.depot(1400);
		c11= new Compte(11);
		c11.depot(500);
		c12= new Compte(12);
		c12.depot(800);

		//Creation client a plusieur compte
		ClientMultiComptes client;
		client= new ClientMultiComptes ("Gence", "Yves", c3);
		System.out.println("nom=" + client.getNom());
		System.out.println("prenom=" + client.getPrenom());
		//Ajout des comptes au client
		client.ajouterCompte(c4);
		client.ajouterCompte(c5);
		client.ajouterCompte(c6);
		client.ajouterCompte(c7);
		client.ajouterCompte(c8);
		client.ajouterCompte(c9);
		client.ajouterCompte(c10);
		client.ajouterCompte(c11);
		client.ajouterCompte(c12);
		//Afficher le solde total
		client.afficherEtatClient();
	}

}
