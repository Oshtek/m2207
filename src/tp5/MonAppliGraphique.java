package tp5;

import java.awt.GridLayout; //Utiliser GridLayout
import java.awt.BorderLayout; //Utiliser BorderLayout
import java.awt.Container; //Utiliser Container de fenetre et Composants graphiques
import java.awt.FlowLayout; //Utiliser Layout Manager

import javax.swing.JButton; //Utiliser des boutons
import javax.swing.JFrame; //Utiliser les fonctions de la classe JFrame
import javax.swing.JLabel; //Utiliser des labels
import javax.swing.JTextField; //Utiliser les TextField


public class MonAppliGraphique extends JFrame{ //Herite de JFrame
	//Attributs
	private JButton bouton0=new JButton("Bouton0");
	private JButton bouton1=new JButton("Bouton1");
	private JButton bouton2=new JButton("Bouton2");
	private JButton bouton3=new JButton("Bouton3");
	private JButton bouton4=new JButton("Bouton4");
	private JLabel monLabel = new JLabel("Je suis un JLabel");;
	private JTextField monTextField= new JTextField("Je suis un JTextField");

	//Constructeurs
	public MonAppliGraphique(){
		super(); // Fait appel au constructeur de la classe JFrame
		this.setTitle("Ma premiere application");
		this.setSize(400,400); //Par defaut, fenetres de taille 400x400
		this.setLocation(20,20); //Position (20,20) de l'ecran
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //gestion fermeture de fenetre
		this.setVisible(true); //Obligatoire en fin de constructeur
		System.out.println("La fenetre a ete creee !"); //Petit message de confirmation lors de la creation de fenetre
		Container panneau = getContentPane(); //Container
		/*Exercice2
			panneau.add(bouton0);
			panneau.add(bouton1);
			panneau.add(bouton2);
			panneau.add(bouton3);
			panneau.add(bouton4);
			panneau.setLayout(new FlowLayout());
		 */

		/*Exercice2.4
			panneau.add(monLabel);
			panneau.add(monTextField);
		 */

		/*
		 * En ajoutant un Layout, le bouton a pris une taille par defaut 
		 * et n'occupe plus la totalite de la fenetre
		 */

		/*Exercice3.1
			panneau.add(bouton0);
			panneau.add(bouton1, BorderLayout.NORTH);
			panneau.add(bouton2, BorderLayout.EAST);
			panneau.add(bouton3, BorderLayout.SOUTH);
			panneau.add(bouton4, BorderLayout.WEST);
		 */

		//Exercice3.2
		panneau.add(bouton0);
		panneau.add(bouton1);
		panneau.add(bouton2);
		panneau.add(bouton3);
		panneau.add(bouton4);
		panneau.setLayout(new GridLayout(3,2));
		this.setVisible(true); //Obligatoire en fin de constructeur
	}

	//Executable
	public static void main(String[] args) {
		MonAppliGraphique app= new MonAppliGraphique();
	}
}
