package tp6;

public class TestExceptions {

	public static void main(String[] args) {

		/*
		  Exercice 1.1 :
		  int x = 2, y = 0;   
		 System.out.println(x/y);   
		 System.out.println("Fin du programme"); 

		 *Le programme ne s'ex�cute pas puisqu'il est impossible de diviser par zero.
		 *La console affiche "Exeception in thread 'main' "
		 */


		/*
		 Exercice 1.2 :
		 int x = 2, y = 0; 
		 try{ //Bloc pour les essai
		 	System.out.println(x/y);   
		 }   
		 catch (Exception e){ //Bloc pour traiter l'exception
		 	System.out.println("Une exception a �t� captur�e");   
		 }   
		 System.out.println("Fin du programme");

		 *Le programme peut d�sormais s'ex�cuter, il parvient � lever l'exception 
		 */

		System.out.println("Exercice 1.3");
		int x=2, y=0;
		try{  
			System.out.println("y/x = " + y/x);  
			System.out.println("x/y = " + x/y); //Cette ligne etant non fonctionnelle, elle sera simplement ignoree  
			System.out.println("Commande de fermeture du programme"); 
			/*
			 * Cette ligne ne s'execute pas puisqu'elle se situe apres une instruction non-fonctionnelle
			 * A la moindre erreur, le programme sort du bloc d'instruction try.
			 */
		}
		catch (Exception e){
			System.out.println("Exception detectee");
		}
		finally { //bloc qui s'execute quoi qu'il arrive
			System.out.println("Commande de fermeture du programme");
		}
	}

}
