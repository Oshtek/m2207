package tp2;

public class TestForme {

	public static void main(String[] args) {
		System.out.println(Forme.getCompteur() + " Objets crees");
		Forme f1;
		f1=new Forme(); //valeurs par d�faut
		Forme f2;
		f2=new Forme("vert", false);
		System.out.println("f1 : " + f1.getCouleur() + "-" + f1.isColoriage());
		System.out.println("f2 : " + f2.getCouleur() + "-" + f2.isColoriage());
		
		System.out.println("\nExercice1.5 :");
		f1.setCouleur("rouge"); //change la couleur de f1
		f1.setColoriage(false); //booleen en faux
		System.out.println("f1 : " + f1.getCouleur() + "-" + f1.isColoriage());
		
		System.out.println("\nExercice1.7 :");
		System.out.println(f1.seDecrire());
		System.out.println(f2.seDecrire());
		
		System.out.println("\nExercice2.5 :");
		Cercle c1;
		c1= new Cercle();
		System.out.println(c1.seDecrire());
		
		System.out.println("\nExercice2.8 :");
		Cercle c2;
		c2=new Cercle(2.5);
		System.out.println(c2.seDecrire());
		
		System.out.println("\nExercice2.10 :");
		Cercle c3;
		c3=new Cercle(3.2,"jaune", false);
		System.out.println(c3.seDecrire());
		
		System.out.println("\nExercice2.11 :");
		//c2
		System.out.println("Aire de c2 : " + c2.calculerAire());
		System.out.println("Perimetre de c2 : " + c2.calculerPerimetre());
		//c3
		System.out.println("Aire de c3 : " + c3.calculerAire());
		System.out.println("Perimetre de c3 : " + c3.calculerPerimetre());
		
		System.out.println("\nExercice3.2 :");
		Cylindre cy1;
		cy1= new Cylindre();
		System.out.println(cy1.seDecrire());
		
		System.out.println("\nExercice3.4");
		Cylindre cy2;
		cy2=new Cylindre(4.2,1.3,"bleu",true);
		System.out.println(cy2.seDecrire());
		
		System.out.println("\nExercice3.5");
		System.out.println("Volume de cy1 : " + cy1.calculerVolume() + " m^3");
		System.out.println("Volume de cy2 : " + cy2.calculerVolume() + " m^3");
		
		System.out.println("\nExercice4");
		System.out.println(Forme.getCompteur() + " Objets crees");
	}

}
