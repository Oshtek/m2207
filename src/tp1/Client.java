package tp1;

public class Client {

	//Attributs
	String nom, prenom;
	private Compte compteCourant;

	//Constructeur
	public Client(String n,String p, Compte compteCourant){
		nom=n;
		prenom=p;
		this.compteCourant=compteCourant;
	}

	//Accesseurs
	public String getNom(){
		return nom;

	}
	public String getPrenom(){
		return prenom;
	}
	//Methodes
	public double getSolde(){
		return compteCourant.getSolde();
	}
	public void afficherSolde(){
		System.out.println("Solde= " + compteCourant.getSolde() + "�");
	}
}
