package tp2;

public class Forme {
	//Attributs
	private boolean coloriage;
	private String couleur;
	private static int compteur = 0;
	
	//Constructeurs
	Forme(){
		coloriage=true;
		couleur="orange";
		compteur++;
	}
	public Forme(String c, boolean r){
		couleur=c;
		coloriage=r;
		compteur++;
	}
	
	//Accesseurs
	String getCouleur(){
		return couleur;
	}
	void setCouleur(String c){
		couleur=c;
	}
	boolean isColoriage(){
		return coloriage;
	}
	void setColoriage(boolean b){
		coloriage=b;
	}
	//methodes
	String seDecrire(){
		return "une Forme de couleur " + getCouleur() + " et de coloriage " + isColoriage();
	}
	public static int getCompteur(){
		return compteur;
	}
}
