package tp2;

public class Cercle extends Forme { //Herite de la classe Forme
	//Attributs
	private double rayon;
	
	//Constructeurs
	Cercle(){
		super(); 
		rayon=1;
	}
	Cercle(double r){
		super();
		rayon=r;
	}
	public Cercle(double r, String couleur, boolean coloriage){
		super(couleur, coloriage);
		rayon=r;
	}
	//Accesseurs
	void setRayon(double r){
		rayon=r;
	}
	double getRayon(){
		return rayon;
	}
	//Methodes
	double calculerAire(){
		double aire = Math.PI * rayon * rayon;
		return aire;
	}
	double calculerPerimetre(){
		double perimetre = 2*Math.PI*rayon;
		return perimetre;	
	}
	String seDecrire(){
		return "Un cercle de rayon " + getRayon() + super.seDecrire();
	}
}
