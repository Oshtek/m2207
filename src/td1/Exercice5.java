package td1;

public class Exercice5 {
	static int i;
	public static void main(String[] args) {
		int[] tableau={1,7,4,6};
		System.out.println("Affichage du tableau");
		for(i=0;i<tableau.length;i++){ //Afficher les elements du tableau
			System.out.println(tableau[i]);
		}
		System.out.println("Inversion du tableau");
		for(i=0;i<tableau.length/2;i++){ /*boucle pour inverser l'ordre des elements du tableau. Je divise la longueur du tableau par 2 
			afin de m'arr�ter au milieu du tableau car les element sont deja permute. Si j'avais garde la longueur par defaut, je serais revenu au point de depart*/
			int temp=tableau[i]; //creation d'une variable temporaire afin d'eviter d'ecraser la valeur courante du tableau
			tableau[i]=tableau[tableau.length-i-1]; //tableau[i] represente notre nouveau tableau. L'autre element represente le tableau originel.
			tableau[tableau.length-i-1]=temp;

		}
		for(i=0;i<tableau.length;i++){ //Afficher les elements du nouveau tableau
			System.out.println(tableau[i]);
		}


	}

}
