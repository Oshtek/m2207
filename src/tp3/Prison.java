package tp3;

public class Prison {
	//Attributs
	private Brigand[] prison;
	private String nom;
	private int nbPrisonnier;

	//Constructeurs
	public Prison(String nom){
		this.nom=nom;
		prison= new Brigand[10];
	}
	//Accesseurs
	String getNom(){
		return nom;
	}
	int getNbPrisonnier(){
		return nbPrisonnier;
	}
	//Methodes
	void mettreEnCellule(Brigand b){
		prison[nbPrisonnier]=b; //tableau de prisonnier
		System.out.println(b.quelEstTonNom() + " est mis dans la cellule " + getNbPrisonnier());
		nbPrisonnier++; //un prisonnier est ajoute, donc le nombre de prisonnier augmente
		compterLesPrisonniers();
	}
	void sortirDeCellule(Brigand b){
		nbPrisonnier=nbPrisonnier-1; //un prisonnier est enleve, donc le nombre de prisonnier diminiue
		prison[nbPrisonnier]=b; //tableau de prisonnier
		System.out.println(b.quelEstTonNom() + " est sorti de sa cellule ");
		compterLesPrisonniers();
	}
	void compterLesPrisonniers(){
		System.out.println("il y a " + getNbPrisonnier() + " brigand(s) dans la prison " + getNom());
	}
}
