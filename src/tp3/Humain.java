package tp3;

public class Humain {
	//Attributs
	protected String boisson;
	protected String nom;
	//Constructeurs
	public Humain(String nom){
		this.nom=nom;
		boisson="lait";
	}
	//Accesseurs
	String getBoisson(){ //acceder a la valeur de boisson
		return boisson;
	}
	String getNom(){ //acceder a la valeur de nom
		return nom;
	}

	//Methodes
	String quelleEstTaBoisson(){ //appel de l'accesseur getBoisson
		return getBoisson();
	}
	String quelEstTonNom(){ //appel de l'accesseur getNom
		return getNom(); 
	}
	void parler(String texte){ //Eviter la redondance system.out.println
		System.out.println(quelEstTonNom() + " - " + texte);
	}
	void sePresenter(){ 
		parler("Bonjour, je suis " + quelEstTonNom() + " et ma boisson preferee est le " + quelleEstTaBoisson() + ".");
	}
	void boire(){
		parler("Ah ! un bon verre de " + quelleEstTaBoisson() + " !  GLOUPS ! ");
	}
}
