package tp3;

public class Histoire {

	public static void main(String[] args) {
		Humain bob; //Nouvel Humain
		bob=new Humain("bob"); //Creation de l'instance pour bob
		bob.sePresenter(); //Bob se presente
		bob.boire(); //Bob boit

		Dame rachel; //nouvelle Dame
		rachel=new Dame("Rachel"); //Creation de l'instance pour Rachel
		rachel.sePresenter(); //Rachel se presente
		rachel.priseEnOtage();//Rachel est en otage
		rachel.estLiberee();//Rachel est liberee

		Brigand gunther;
		gunther= new Brigand("gunther"); //Creation de l'instance pour Gunther
		gunther.setBoisson("rhum"); //Changement de boisson pour Gunther
		gunther.sePresenter(); //Gunther se presente
		gunther.enleve(rachel); //Gunther eneleve Rachel

		Cowboy simon; //Nouveau Cowboy
		simon=new Cowboy("Simon");  //Creation de l'instance pour Simon
		simon.sePresenter(); //Simon se presente
		simon.tire(gunther); //Simon tire sur gunther

		System.out.println("\nExercice7.5 (scenario complet) :");
		rachel.sePresenter();
		gunther.sePresenter();
		gunther.enleve(rachel);
		rachel.priseEnOtage();
		gunther.sePresenter();
		rachel.sePresenter();
		simon.tire(gunther);
		rachel.estLiberee();
		rachel.sePresenter();

		System.out.println("\nExercice8 :");
		Sherif marshall; //Nouveau Sherif
		marshall=new Sherif("Marshall"); //Creation de l'instance pour Marshall
		marshall.setArrestation(20); //Marshall a effectue 20 arrestations
		marshall.sePresenter();

		System.out.println("\nExercici8.4 :");
		marshall.sePresenter();
		marshall.coffrer(gunther); //Marshall arrete Gunther
		gunther.emprisonner(marshall); //Gunther se fait avoir par Marshall
		marshall.sePresenter();

		System.out.println("\nExercice9 :");
		Cowboy clint= new Sherif("Clint"); //Sherif herite de cowboy donc c'est possible
		clint.sePresenter(); //Clint se presente

		/*
		 * Nous pouvons appeler toutes les methodes de la classe Cowboy mais pas celles de la classe Sherif
		 *  Une classe fille peut utiliser les methodes de la classe mere mais pas l'inverse 
		 *  car elles ne sont pas definies dans cette derniere
		 */

		System.out.println("\nExercice10 :");
		Prison esperanza;
		esperanza= new Prison ("Fort Esperanza");
		esperanza.mettreEnCellule(gunther); //gunther est mis en cellule
		esperanza.sortirDeCellule(gunther); //gunther sort de cellule

		Brigand jason;
		jason=new Brigand("jason");
		esperanza.mettreEnCellule(jason); //Jason recupere la cellule de gunther

		/*
		 * Afin de realiser un Cachot auquel nous pouvons enfermer tout type de personnage
		 * nous creerons un cachot faisant appel a la classe humain.
		 * Etant donne qu'un personnage est un humain, tout objet lie a la classe mere humain
		 * pourra etre enferme dans le cachot.
		 */

		System.out.println("\nExercice12 :");
		Cachot cachot;
		cachot = new Cachot("Cachot");
		cachot.mettreEnCellule(marshall);
		cachot.sortirDeCellulle(marshall);
	}

}
