package tp2;

public class Cylindre extends Cercle { //herite de la classe Cercle
	//Attributs
	private double hauteur;
	//Constructeurs
	Cylindre (){
		super();
		hauteur=1; //valeur par defaut
	}
	Cylindre(double hauteur, double r, String couleur, boolean coloriage){
		super(r, couleur, coloriage); //appelle les 3 autres attributs
		this.hauteur=hauteur;
	}
	
	//Accesseurs
	double getHauteur(){
		return hauteur;
	}
	void setHauteur(double h){
		hauteur=h;
	}
	//Methodes
	String seDecrire(){
		return "un Cylindre d'une hauteur de " + getHauteur() +super.seDecrire();
	}
	double calculerVolume(){
		double volume=super.calculerAire()*hauteur;
		return volume;
	}
}
