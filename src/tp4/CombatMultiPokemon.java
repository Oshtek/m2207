package tp4;

import java.util.Scanner;

public class CombatMultiPokemon {

	public static void main(String[] args) {
		Scanner scan=new Scanner (System.in);
		Pokemon j1p1=new Pokemon();
		j1p1.saisieNom1Joueur1();
		Pokemon j1p2=new Pokemon();
		j1p2.saisieNom2Joueur1();
		Pokemon j2p1=new Pokemon();
		j2p1.saisieNom1Joueur2();
		Pokemon j2p2=new Pokemon();
		j2p2.saisieNom2Joueur2();
		j1p1.sePresenter();
		j1p2.sePresenter();
		j2p1.sePresenter();
		j2p2.sePresenter();
		j1p1.MultiCombat(j1p1, j1p2, j2p1, j2p2);
	}

}
