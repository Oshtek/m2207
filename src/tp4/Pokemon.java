package tp4;
import java.util.Scanner; //Permet la capture du clavier
public class Pokemon {
	//Attributs
	private int energie;
	private int maxEnergie;
	private String nom;
	private boolean alive;
	private boolean furie;
	private int cycle;
	private int puissance;
	private int perte;
	private static int round;
	private int fatigue;
	private Scanner scan = new Scanner(System.in);
	private Scanner scanchoix = new Scanner(System.in);
	private int action;
	private int choix;

	//Constructeurs
	public Pokemon(String nom){
		this.nom=nom;
		maxEnergie= 50 + (int)(Math.random() * ((90 - 50) + 1)); //Niveau max d'energie compris aleatoirement entre 50 et 90
		energie=30 + (int)(Math.random() * ((maxEnergie - 30) + 1)); //Niveau d'energie compris aleatoirement entre 30 et maxEnergie
		puissance=3 + (int)(Math.random() * ((10 - 3) + 1)); //Niveau max de puissance compris aleatoirement entre 3 et 10
		alive=true; //Pokemon par defaut en vie
	}
	public Pokemon(){
		maxEnergie= 50 + (int)(Math.random() * ((90 - 50) + 1)); //Niveau max d'energie compris aleatoirement entre 50 et 90
		energie=30 + (int)(Math.random() * ((maxEnergie - 30) + 1)); //Niveau d'energie compris aleatoirement entre 30 et maxEnergie
		puissance=3 + (int)(Math.random() * ((10 - 3) + 1)); //Niveau max de puissance compris aleatoirement entre 3 et 10
		alive=true; //Pokemon par defaut en vie
	}
	//Accesseurs
	String getNom(){
		return nom;
	}
	int saisieAction(){ //Fait intervenir un scanner pour determiner l'action a effectuer
		System.out.println(getNom() + " : Attaquer(0) ou manger(1) ?" );
		return action=scan.nextInt();
	}
	int choisirAdversaire(Pokemon Adversaire1,Pokemon Adversaire2){
		System.out.println(" Quel adversaire attaquer ? " + Adversaire1.getNom() + "(0) ou " + Adversaire2.getNom() + "(1)");
		return choix=scanchoix.nextInt();
	}
	String saisieNom1(){ //Fait intervenir le Scanner pour le premier pokemon
		System.out.println("Saisissez le nom du premier combattant : ");
		return nom=scan.nextLine();
	}
	String saisieNom2(){ //Fait intervenir le Scanner pour un second pokemon
		System.out.println("Saisissez le nom du second combattant : ");
		return nom=scan.nextLine();
	}
	String saisieNom1Joueur1(){ //Fait intervenir le Scanner pour le premier pokemon du joueur 1
		System.out.println("Saisissez le nom du premier combattant du joueur 1 : ");
		return nom=scan.nextLine();
	}
	String saisieNom2Joueur1(){ //Fait intervenir le Scanner pour le deuxieme pokemon du joueur 2
		System.out.println("Saisissez le nom du deuxieme combattant du joueur 1 : ");
		return nom=scan.nextLine();
	}
	String saisieNom1Joueur2(){ //Fait intervenir le Scanner pour le premier pokemon du joueur 1
		System.out.println("Saisissez le nom du premier combattant du joueur 2 : ");
		return nom=scan.nextLine();
	}
	String saisieNom2Joueur2(){ //Fait intervenir le Scanner pour le premier pokemon du joueur 1
		System.out.println("Saisissez le nom du deuxieme combattant du joueur 2 : ");
		return nom=scan.nextLine();
	}
	int getCycle(){
		return cycle;
	}
	int getEnergie(){
		return energie;
	}
	int getMaxEnergie(){
		return maxEnergie;
	}
	int setEnergie(int energie){
		return this.energie=energie;
	}
	int getPuissance(){
		return puissance;
	}
	int getFatigue(){
		return fatigue;
	}
	static int getRound(){
		return round;
	}

	//Methodes
	void parler(String texte){ //Eviter la redondance system.out.println
		System.out.println(getNom() + " - " + texte);
	}
	void sePresenter(){
		parler("Je suis " + getNom()+ ", j'ai " + getEnergie() + " points d'energie (" + getMaxEnergie()+ " max) et une puissance de " + getPuissance());
	}
	void narration(String texte){  //Eviter la redondance system.out.println
		System.out.println("* " + texte + " *");
	}
	void manger(){
		energie=energie+(10 + (int)(Math.random() * ((30 - 10) + 1))); //gain max d'energie compris aleatoirement entre 10 et 30
		if (energie>=maxEnergie){ //On determine la limite des soins
			narration("Le pokemon n'a plus faim, et jette la nourriture par terre");
		}
		else narration(getNom() + " passe a " + getEnergie() + " points d'energie");
		//Manger fait recuperer un peu de puissance
		puissance=puissance + (int)(Math.random() * (((puissance+3)- puissance) + 1)); //Puissance comprise entre la puissance de base et la puissance augment�e de 3
	}
	void vivre(){ //Perte d'energie au fil du temps
		energie=energie-(20 + (int)(Math.random() * ((40 - 20) + 1))); //perte max d'energie compris aleatoirement entre 20 et 40
	}
	boolean isAlive(){ //Verification de l'etat du pokemon
		if (energie>0){ //energie positive, le pokemon est vivant
			return alive=true;
		}
		else return alive=false; //le pokemon est mort
	}
	void cycles(){
		while (energie>0){ 
			vivre();
			cycle++;
			if (energie<maxEnergie){
				manger();
				cycle++;
			}
		}
		narration(getNom() + " a vecu " + getCycle() + " cycles !");
	}
	void perdreEnergie(int perte){
		if (furie==true){ //En mode furie, les degats recus sont multiplie par 1.5
			perte=(int) (perte*1.5); //cast en int afin de tronquer la valeur
		}
		energie=energie-perte;
	}
	void attaquer(Pokemon adversaire){
		if (round==1){ //Au premier round il n'y a pas de fatigue
			fatigue=0;
		}
		else 
			fatigue=0 +(int) (Math.random() * ((1-0)+1)); //Niveau max d'energie compris aleatoirement entre 0 et 1
		adversaire.perdreEnergie(puissance);
		puissance=puissance-fatigue;

		//Boucle if pour eviter de passer l'attaque en dessous de zero
		if (puissance<=0){
			puissance=1;
		}

		//Boucle if pour verifier la furie du pokemon
		if (energie<(25*maxEnergie/100) && furie==false){ 
			furie=true; //Activation du mode furie
			narration(getNom() + " est en furie ! degats doubles !!!"); 
			puissance=puissance*2; //En furie la puissance est doublee
		}
		if (adversaire.getEnergie()<=0){ //Affiche un message afin de prevenir le joueur de ne pas s'acharner 
			narration(adversaire.getNom() + " est KO !");
			adversaire.alive=false;
		}
	}
	void combat(Pokemon actuel, Pokemon adversaire){
		System.out.println("-------------------------");
		while (adversaire.energie>0 && energie>0){
			round++; //Incrementation de la variable a chaque round
			actuel.attaquer(adversaire);
			adversaire.attaquer(actuel);
			narration("Round " + getRound() + " " + getNom() + " (en) " + getEnergie() + " (atk) " + getPuissance() + " "+ adversaire.getNom() + " (en) " + adversaire.getEnergie() + " (atk) " + adversaire.getPuissance());
			//Verification de l'etat des combattants
			adversaire.isAlive();
			isAlive();
		}
		System.out.println("-------------------------");
		if (adversaire.alive==false && actuel.alive==true){ //1er cas, actuel vainqueur
			narration(actuel.getNom() + " gagne en " + getRound() + " rounds !");
		}
		else if (actuel.alive==false && adversaire.alive==true){ //2eme cas, adversaire vainqueur
			narration(adversaire.getNom() + " gagne en " + getRound() + " rounds !");
		}
		else narration("Match nul !"); //Aucune des conditions est respecte, match nul !
	}
	void CombatEntreJoueurs(Pokemon Joueur1, Pokemon Joueur2){
		while (Joueur1.getEnergie()>0 && Joueur2.getEnergie()>0){
			round++;
			System.out.println("-------------------------");
			System.out.println("Round " +getRound());
			narration("Etat des Combattants : " + Joueur1.getNom() + " (en) " + Joueur1.getEnergie() + " (atk) " + Joueur1.getPuissance() + " vs " + Joueur2.getNom() + " (en) " + Joueur2.getEnergie() + " (atk) " + Joueur2.getPuissance());
			Joueur1.saisieAction();
			if (Joueur1.action==0){
				Joueur1.attaquer(Joueur2);
				narration(Joueur2.getNom() +" est maintenant a " + Joueur2.getEnergie() + " points d'energie !");
			}
			else if (Joueur1.action==1){
				Joueur1.manger();
			}
			else {
				Joueur1.saisieAction();
			}
			Joueur2.saisieAction();
			if (Joueur2.action==0){
				Joueur2.attaquer(Joueur1);
				narration(Joueur1.getNom() +" est maintenant a " + Joueur1.getEnergie() + " points d'energie !");
			}
			else if (Joueur2.action==1){
				Joueur2.manger();
			}
			else {
				Joueur2.saisieAction();
			}
			Joueur1.isAlive();
			Joueur2.isAlive();
		}

		//Verification conditions fin de match
		System.out.println("-------------------------");
		if (Joueur1.alive==false && Joueur2.alive==true){ //1er cas, actuel vainqueur
			narration(Joueur2.getNom() + " gagne en " + getRound() + " rounds !");
		}
		else if (Joueur2.alive==false && Joueur1.alive==true){ //2eme cas, adversaire vainqueur
			narration(Joueur1.getNom() + " gagne en " + getRound() + " rounds !");
		}
		else narration("Match nul !"); //Aucune des conditions est respecte, match nul !
	}
	void MultiCombat(Pokemon j1p1, Pokemon j1p2, Pokemon j2p1, Pokemon j2p2){
		while ((j1p1.getEnergie()>0 || j1p2.getEnergie()>0) && (j2p1.getEnergie()>0 || j2p2.getEnergie()>0)){
			round++;
			System.out.println("-------------------------");
			System.out.println("Round " +getRound());
			narration("Etat des Combattants : " + j1p1.getNom() + " (en) " + j1p1.getEnergie() + " (atk) " + j1p1.getPuissance() + " et " + j1p2.getNom() + " (en) " + j1p2.getEnergie() + " (atk) " + j1p2.getPuissance() + " vs " + j2p1.getNom() + " (en) " + j2p1.getEnergie() + " (atk) " + j2p1.getPuissance() + " et " + j2p2.getNom() + " (en) " + j2p2.getEnergie() + " (atk) " + j2p2.getPuissance() );

			//Champs d'action pour j1p1
			if (j1p1.getEnergie()>0){ //Si il est en etat de se battre
				j1p1.saisieAction();
				if (j1p1.action==0){
					j1p1.choisirAdversaire(j2p1, j2p2);
					if (j1p1.choix==0){
						j1p1.attaquer(j2p1);
						narration(j2p1.getNom() + " passe a " + j2p1.getEnergie() + " points d'energie !");
					}
					else if (j1p1.choix==1){
						j1p1.attaquer(j2p2);
						narration(j2p2.getNom() + " passe a " + j2p2.getEnergie() + " points d'energie !");
					}
					else j1p1.choisirAdversaire(j2p1, j2p2);
				}
				else if (j1p1.action==1){
					j1p1.manger();
				}
				else j1p1.saisieAction();
			}

			//Champs d'action pour j2p1
			if (j2p1.getEnergie()>0){ //Si il est en etat de se battre
				j2p1.saisieAction();
				if (j2p1.action==0){
					j2p1.choisirAdversaire(j1p1, j1p2);
					if (j2p1.choix==0){
						j2p1.attaquer(j1p1);
						narration(j1p1.getNom() + " passe a " + j1p1.getEnergie() + " points d'energie !");
					}
					else if (j2p1.choix==1){
						j2p1.attaquer(j1p2);
						narration(j1p2.getNom() + " passe a " + j1p2.getEnergie() + " points d'energie !");
					}
					else j2p1.choisirAdversaire(j1p1, j1p2);
				}
				else if (j2p1.action==1){
					j2p1.manger();
				}
				else j2p1.saisieAction();
			}

			//Champs d'action pour j1p2
			if (j1p2.getEnergie()>0){ //Si il est en etat de se battre
				j1p2.saisieAction();
				if (j1p2.action==0){
					j1p2.choisirAdversaire(j2p1, j2p2);
					if (j1p2.choix==0){
						j1p2.attaquer(j2p1);
						narration(j2p1.getNom() + " passe a " + j2p1.getEnergie() + " points d'energie !");
					}
					else if (j1p2.choix==1){
						j1p2.attaquer(j2p2);
						narration(j2p2.getNom() + " passe a " + j2p2.getEnergie() + " points d'energie !");
					}
					else j2p1.choisirAdversaire(j2p1, j2p2);
				}
				else if (j1p2.action==1){
					j1p2.manger();
				}
				else j1p2.saisieAction();
			}
			
			//Champs d'action pour j2p2
			if (j2p2.getEnergie()>0){ //Si il est en etat de se battre
				j2p2.saisieAction();
				if (j2p2.action==0){
					j2p2.choisirAdversaire(j1p1, j1p2);
					if (j2p2.choix==0){
						j2p2.attaquer(j1p1);
						narration(j1p1.getNom() + " passe a " + j1p1.getEnergie() + " points d'energie !");
					}
					else if (j2p2.choix==1){
						j2p2.attaquer(j1p2);
						narration(j1p2.getNom() + " passe a " + j1p2.getEnergie() + " points d'energie !");
					}
					else j2p1.choisirAdversaire(j1p1, j1p2);
				}
				else if (j2p2.action==1){
					j2p2.manger();
				}
				else j2p2.saisieAction();
			}
			
			//Verification etat des combattants a la fin du round
			j1p1.isAlive();
			j1p2.isAlive();
			j2p1.isAlive();
			j2p2.isAlive();
		}

		//Verification conditions fin de match
		System.out.println("-------------------------");
		if ((j1p1.alive==false && j1p2.alive==false) && (j2p1.alive==true || j2p2.alive==true)){
			narration("Joueur 2 gagne en " + getRound() + " rounds !");
		}
		else if ((j2p1.alive==false && j2p2.alive==false) && (j1p1.alive==true || j1p2.alive==true)){
			narration("Joueur 1 gagne en " + getRound() + " rounds !");
		}
		else narration("Match nul !");
	}
}

