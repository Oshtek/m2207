package tp1;

public class ClientMultiComptes {
	//Attributs
	String nom, prenom;
	private Compte[] tabcomptes = new Compte[10];
	int i;
	int NbComptes;
	double solde;

	//Constructeur
	public ClientMultiComptes(String nom, String prenom, Compte compte){
		this.nom=nom;
		this.prenom=prenom;
		NbComptes=1;
		tabcomptes[0]=compte;

	}
	//Accesseurs
	public String getNom(){
		return nom;

	}
	public String getPrenom(){
		return prenom;
	}
	//Methodes
	public void ajouterCompte(Compte c){ //A chaque comptes ajoute, Nbcompte est incremente
		tabcomptes[NbComptes]=c;
		NbComptes++;
	}
	public double getSolde(){ //boucle for afin de calculer la somme des valeurs de chaque champs
		for (i=0;i<NbComptes;i++){
			solde+=tabcomptes[i].getSolde();
		}
		return solde;
	}
	public void afficherEtatClient(){ //Afficher le solde total
		System.out.println("Solde total des comptes :" +getSolde()+ "�");
	}
}
