package td3;

public class Chien extends Animal {
	public String affiche(){
		return "Je suis un Chien";
	}
	public String cri(){
		return "Ouaf ! Ouaf !";
	}
	/* public String origine(){ On ne peut pas ecraser la methode finale, il faut avoir la meme methode qu'animal
		return null; 
	} */
}