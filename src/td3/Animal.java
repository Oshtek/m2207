package td3;

public class Animal { //Animal est fonctionnel sans constructeur car il n'a pas d'attributs
	public String affiche(){
		return "Je suis un animal";
	}
	public String cri(){
		return "...";
	}
	public final String origine(){
		return "La classe Animal est la mere de toutes les classes!";
	}
}
