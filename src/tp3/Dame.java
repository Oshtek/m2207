package tp3;

public class Dame extends Humain{ //Herite de la classe Humain
	//Attributs
	private boolean libre;
	//Constructeurs
	public Dame(String nom){
		super(nom);
		boisson="Martini";
		libre=true;
	}
	//Methodes
	void priseEnOtage(){
		parler("Au secours !");
		libre=false;
	}
	void estLiberee(){
		parler("Merci Cowboy !");
		libre=true;
	}
	String quelEstTonNom(){
		return "Miss " + super.getNom(); 
	}
	void parler(String texte){
		System.out.println(super.quelEstTonNom() + " - " + texte);
	}
	String liberte(){ //Afin d'eviter de retourner la valeur booleenne de la variable
		if (libre==true){
			return "libre";
		}
		else {
			return "kidnappee";
		}
	}
	void sePresenter(){
		super.sePresenter();
		parler("Actuellement, je suis " + liberte());
	}
}
