package tp1;

public class MaBanque {

	public static void main(String[] args) {
		Compte compte;
		compte= new Compte(1);
		System.out.println("compte " + compte.getNumero());
		System.out.println("Montant du decouvert : " + compte.getDecouvert() + "�");
		//Modifier le Decouvert
		compte.SetDecouvert(100);
		System.out.println("Montant du decouvert : " + compte.getDecouvert() + "�");
		//Afficher le solde
		compte.afficherSolde();
		//Depot du montant sur le solde
		compte.depot(100);
		compte.afficherSolde();
		//Retrait 
		compte.retrait(200);
		//Afficher nouveau solde
		compte.afficherSolde();

		System.out.println("\nNouveau compte");
		Compte c2;
		c2= new Compte(2);
		c2.depot(1000);
		c2.afficherSolde();
		c2.retrait(600);
		c2.afficherSolde();
		c2.retrait(700);
		c2.SetDecouvert(500);
		c2.retrait(700);
		c2.afficherSolde();

		Client client1;
		client1 = new Client("Kimberley", "tartine", c2);
		System.out.println("\n Solde du client:");
		client1.afficherSolde();

		System.out.println("\nExercice 5.2");
		Compte cp1;
		cp1= new Compte(13);
		cp1.depot(1000);
		cp1.afficherSolde(); //afficher solde cp1
		Compte cp2;
		cp2= new Compte(0);
		cp2.afficherSolde(); //afficher solde cp2
		cp1.virer(cp2, 600); //virement de 600 euros du compte cp1 vers cp2
		cp1.afficherSolde();
		cp2.afficherSolde();
		cp1.virer(cp2, 600);
		cp1.afficherSolde();
		cp2.afficherSolde();

		/*
		 * Ayant un decouvert a 0 et un solde de cp1 etant a 400, il est donc impossible de retirer de nouveau 600 euros
		 */
	}
}
