package td3;

public class TestAnimal {

	public static void main(String[] args) {
		Animal animal;
		animal= new Animal(); //Par defaut, il y a un constructeur vide cree par java
		System.out.println(animal.toString()); //Animal traduit en hexa
		System.out.println(animal.affiche()); //En mettant private, les methodes affiche et cri sont invisibles
		System.out.println(animal.cri());

		Chien chien; //creer un chien
		chien= new Chien();
		System.out.println(chien.affiche());
		System.out.println(chien.cri());

		//test Origine
		System.out.println(animal.origine());
		System.out.println(chien.origine());

		Chat chat; //creer un chat
		chat=new Chat();
		System.out.println(chat.affiche());
		
		System.out.println("\nExercice3.4");
		System.out.println(animal.cri());
		System.out.println(chien.cri());
		System.out.println(chat.cri());
		System.out.println(chat.miauler());
		/*System.out.println(animal.miauler()); 
		 * miauler n'est pas definit dans la classe animal, cela ne marche pas
		 */
	}

}
