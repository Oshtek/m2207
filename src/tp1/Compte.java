package tp1;

public class Compte {

	//Attributs
	private int numero;
	private double solde=0, decouvert=0;

	//Constructeur
	public Compte(int numero){
		this.numero=numero;
	}
	//Accesseurs
	public void SetDecouvert(double montant){
		decouvert=montant;

	}
	public double getDecouvert(){
		return decouvert;

	}
	public int getNumero(){
		return numero;

	}
	public double getSolde(){
		return solde;

	}

	//Methodes
	public void afficherSolde(){
		System.out.println("Solde= " + solde + "�");
	}
	public void depot(double montant){
		solde=solde+montant;
	}
	public String retrait(double montant){
		if (montant>solde+decouvert) {
			System.out.println("Retrait refus�");
		}
		else {
			System.out.println("Retrait accept�");
			solde=solde-montant;
		}
		return null;
	}
	public String virer(Compte destinataire, double montant){
		if (solde+decouvert>montant){
			System.out.println("Virement accepte");
			solde-=montant;
			destinataire.solde+=montant;
		}
		else {
			System.out.println("Virement refuse");

		}
		return null;
	}
}
