package tp3;

public class Brigand extends Humain { //herite de la classe Humain
	//Attributs
	private int kidnapping;
	private String look;
	private boolean prison;
	private int recompense;
	//Constructeurs
	public Brigand(String nom){
		super(nom);
		prison=false;
		look="mechant";
		recompense=100;
		kidnapping=0;
		boisson="cognac";
	}
	//Accesseurs
	int getRecompense(){ //accede a la valeur de la variable Recompense
		return recompense;
	}

	String setBoisson(String boisson){ //modifie la valeur de la variable boisson
		return this.boisson=boisson;
	}
	//Methodes
	String quelEstTonNom(){ //appel de l'accesseur de la classe mere
		return super.getNom() + " le Mechant "; 
	}
	void parler(String texte){ //appel de l'accesseur de la classe mere
		System.out.println(super.quelEstTonNom() + " - " + texte);
	}
	void sePresenter(){ //appel de l'accesseur de la classe mere
		super.sePresenter();
		parler("J'ai l'air " + look + " et j'ai enleve " + kidnapping + " dames");
		parler("Ma tete est mise a prix " + recompense + "$ !!");
	}
	void enleve(Dame dame){
		kidnapping++;
		recompense=recompense+100;
		parler("ah ah  " + dame.getNom() + ", tu es ma prisoniere !");
	}
	void emprisonner(Sherif s){
		parler("Damned, je suis fait !" + s.getNom() + "tu m'as eu !");
	}
}
