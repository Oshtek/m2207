package tp3;

public class Sherif extends Cowboy { //Herite de la classe Cowboy
	//Attributs
	private int arrestation;

	//Constructeurs
	public Sherif(String nom){
		super(nom);
		arrestation=0;
	}

	//Accesseurs
	int getArrestation(){ //acceder a la valeur de la variable arrestation
		return arrestation;
	}
	int setArrestation(int arrestation){ //modifier la valeur de la variable arrestation
		return this.arrestation=arrestation;
	}

	//Methodes
	String quelEstTonNom(){ //appel de l'accesseur de la classe mere
		return "Sherif " + super.getNom(); 
	}
	void sePresenter(){ //appel de l'accesseur de la classe mere
		super.sePresenter();
		parler("j'ai capture " + getArrestation() + " brigand(s) !"); //pas du super afin d'eviter une redondance
	}
	void coffrer(Brigand b){
		b.emprisonner(this); //this fais reference a sherif pour eviter les conflits
		parler("Au nom de la loi, je vous arrete, " + b.getNom()+ " !");
		arrestation++;
	}
}
